import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { lastValueFrom} from 'rxjs';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;
const idEmpresa = environment.idEmpresa;
const idOficina = environment.idOficina 

@Injectable({
  providedIn: 'root'
})

export class RutService {

  constructor(private http: HttpClient) {}

  headers = new HttpHeaders({
    'accept': 'application/json',
    'Content-Type': 'application/json',
  })

  async rutTurno(rut:any, idSerieSelect:any): Promise<any> {
    let tokenid:any =  localStorage.getItem('tokenid');
    this.headers = this.headers.set('Authorization', `Bearer ${tokenid}`);

    let data = {
      idCtip: "RUT",
      idEmpresa: idEmpresa,
      idOficina: idOficina,
      idSerie: idSerieSelect,
      pin:rut
    }

    try {
      return await lastValueFrom(this.http.post(`${apiUrl}/Tol/v2/reservar`, data, {headers: this.headers}));
    } catch (error) {
      return { status: false, message: 'Error al ejecutar la petición.' };
    }
}

}
