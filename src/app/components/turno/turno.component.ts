import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { TurnoService } from 'src/app/core/services/turno.service';
import { HubService } from 'src/app/core/services/hub.service';
import { SweetalertService } from 'src/app/core/misc/sweetalert.service';

import { LoadingComponent } from '../shared/loading/loading.component';
@Component({
  selector: 'app-turno',
  standalone: true,
  imports: [CommonModule,RouterModule,LoadingComponent],
  templateUrl: './turno.component.html',
  styleUrls: ['./turno.component.css']
})

export class TurnoComponent implements OnInit {

  constructor(
    private turnoService:TurnoService,
    private hubService:HubService,
    private router:Router,
    private sweetAlert:SweetalertService
    ) { }
    
  loading:boolean = false;
  turno:any = {};
  poolTurno:any = []

  ngOnInit(): void {
    if(!localStorage.getItem('turnoticket')){
      this.router.navigate(['/']);
     }else{
      this.verTurno();
     }
  }

  async verTurno(){
    this.loading = true;
    let turnoticket = localStorage.getItem('turnoticket');

    let res = await this.turnoService.obtenerTurno(turnoticket);

      if(res.listaTurno){
        this.turno = res.listaTurno[0];
        this.poolQr(this.turno)
        this.loading = false;
      }else if(res.status == false){
        this.loading = false;
        this.sweetAlert.swalConfirm(res.message).then(resolve =>{
          if(resolve)
            this.verTurno();
         });
      }
  }

  async poolQr(data:any){
    let res = await this.turnoService.poolqr(data);

    if(res.listaPoollistas){
      this.poolTurno = res.listaPoollistas.length > 0 ? res.listaPoollistas[0] : [];
      this.hubService.iniciarConexion(this.turno.idOficina);

       if (this.turno != {} && this.turno.estado == 'A' || this.turno.estado == 'O') {
         localStorage.clear();
         this.hubService.stop()
       }else{
         this.getData();
       }
    }else if(res.status == false){
      this.loading = false;
      this.sweetAlert.swalConfirm(res.message).then(resolve =>{
        if(resolve)
        this.verTurno();
       });
    }
  }

  getData() {
    this.hubService.signalReceived.subscribe({
      next:  (message: any) => {
        if(this.turno.idTurno == message.idTurno){
          this.turno.estado = message.estado;
          this.turno.modulo = message.modulo;
          new Audio('assets/mp3/notifica.mp3').play();

          if (this.turno.estado == 'A' || this.turno.estado == 'O') {
            localStorage.clear();
            this.hubService.stop();
          }
        }
    }});
  }

}
