import { EventEmitter, Injectable } from "@angular/core";
import * as signalR from "@microsoft/signalr";
import { environment } from "src/environments/environment";

const apiUrl = environment.apiHub;
const idEmpresa = environment.idEmpresa 

@Injectable({
  providedIn: 'root'
})
export class HubService {

  private hubConnection!: signalR.HubConnection; 
  public respuestaRecibirPedido= new EventEmitter<string>();
  public signalReceived = new EventEmitter<string>();

  constructor() { }

 iniciarConexion(idOficina:string) {
  this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(`${apiUrl}/v2/turno-hub`,{
        skipNegotiation: true,
        transport: signalR.HttpTransportType.WebSockets
    })
      .withAutomaticReconnect({
      nextRetryDelayInMilliseconds: retryContext => {
          if (retryContext.elapsedMilliseconds < 5000) {
              return Math.random() * 500;
          } else {
              return null;
          }
      }
  })
  .configureLogging(signalR.LogLevel.None)
  .build();

    this.hubConnection
    .start()
    .then(() => console.log('Connection Hub'))
    .catch(err => console.log('Error de hub: ' + err));

    let oficina = `oficina${idOficina}${idEmpresa}`;

    this.hubConnection.on(oficina, (data: any) => {
      this.signalReceived.emit(data);
    });
  }

  
  stop(){
    this.hubConnection.stop();
  }

}
