import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { LoadingComponent } from '../shared/loading/loading.component';

import { Component, OnInit } from '@angular/core';
import { SeriesService } from 'src/app/core/services/series.service';
import { SweetalertService } from 'src/app/core/misc/sweetalert.service';

@Component({
  selector: 'app-series',
  standalone: true,
  imports: [CommonModule,RouterModule,LoadingComponent],
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {

  listaSeries:any[]=[];
  loading:boolean = false;

  constructor(
    private seriesService:SeriesService,
    private router:Router,
    private sweetAlert:SweetalertService
    ) { }

  ngOnInit(): void {
    if(localStorage.getItem('turnoticket')){
      this.router.navigate(['/turno']);
     }else{
      this.loginAuth();
     }
  }

 async loginAuth() {
  this.loading = true;
    let res = await this.seriesService.loginToken()
    if (res.tokenid) {
      localStorage.setItem('tokenid', res.tokenid);
      this.listarSeries();
    }else if(res.status == false){
      this.loading = false;
      this.sweetAlert.swalConfirm(res.message).then(resolve =>{
       if(resolve)
         this.loginAuth();
      });
    }
  }

  async listarSeries(){
    let res = await this.seriesService.listarSeries();
    if(res.listaServicios){
      this.listaSeries = res.listaServicios
      this.loading = false;
    }else if(res.status == false){
      this.loading = false;
      this.sweetAlert.swalConfirm(res.message).then(resolve =>{
        if(resolve)
          this.loginAuth();
       });
    }
  }

  selectService(id:string){
    localStorage.setItem('idSerieSelect',id)
    this.router.navigate(['/rut']);
  }

}
