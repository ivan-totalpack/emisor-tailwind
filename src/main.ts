import { bootstrapApplication } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { provideRouter } from '@angular/router';
import { AppComponent } from './app/app.component';
import { routes } from './app/app-routing';

import { enableProdMode, importProvidersFrom } from '@angular/core';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers:[
    provideRouter(routes),
    importProvidersFrom(HttpClientModule)
  ]
 }).
  catch((err) => console.error(err));
