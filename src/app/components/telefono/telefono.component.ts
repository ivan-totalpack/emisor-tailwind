import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule, FormGroup, FormControl, Validators} from '@angular/forms'

@Component({
  selector: 'app-telefono',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule],
  templateUrl: './telefono.component.html',
  styleUrls: ['./telefono.component.css']
})
export class TelefonoComponent implements OnInit {

  constructor() { }

  form = new FormGroup({
    telefono: new FormControl('', [Validators.required, Validators.pattern(/^((\\+91-?)|0)?[0-9]{9}$/)])
  })
  submit: boolean = false;

  ngOnInit(): void {
  }

  login(){
    this.submit = true
    if (this.form.valid && !this.form.errors) {
      console.log(this.form.value);
      
      alert('datos enviados')
    }
  }

}
