import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormGroup, FormControl, Validators } from '@angular/forms'
import { LoadingComponent } from '../shared/loading/loading.component';

import { ValidaRutService } from 'src/app/core/misc/valida-rut.service';
import { RutService } from 'src/app/core/services/rut.service';
import { TurnoService } from 'src/app/core/services/turno.service';
import { Router } from '@angular/router';
import { SweetalertService } from 'src/app/core/misc/sweetalert.service';

@Component({
  selector: 'app-rut',
  standalone: true,
  imports: [CommonModule,ReactiveFormsModule,LoadingComponent],
  templateUrl: './rut.component.html',
  styleUrls: ['./rut.component.css']
})
export class RutComponent implements OnInit {

  loading:boolean = false;
  form = new FormGroup({ rut: new FormControl('', Validators.required)});
  validatedRut: boolean = false;
  submit: boolean = false;

  constructor(
    private validaRutService: ValidaRutService,
    private rutService:RutService,
    private turnoService:TurnoService,
    private router:Router,
    private sweetAlert:SweetalertService
    ) { }

  ngOnInit(): void {
    if(localStorage.getItem('turnoticket')){
      this.router.navigate(['/turno']);
     }
  }

  formatRut() {
    this.validatedRut = this.validateRut();
    this.form.patchValue({
      rut: this.validaRutService.formatearRut(this.form.value.rut)
    });
  }

  private validateRut() {
    const userRut = this.limpiarRut();
    return this.validaRutService.validarRut(userRut);
  }

  private limpiarRut() {
    return this.form.value.rut?.replace(/\./g, "").replace("-", "");
  }


  async rutTurno(){
    this.submit = true
    if (this.form.valid && !this.form.errors && this.validatedRut) {

      this.loading = true;
      let idSerieSelect = localStorage.getItem('idSerieSelect');
      let rut = this.form.value.rut?.replace(/\./g, "").replace(".", "");

     let res = await this.rutService.rutTurno(rut,idSerieSelect);
        if (res.info.coderr != 0) {
          this.loading = false;
          this.sweetAlert.toast(`Error ${res.info.coderr}, ${res.info.msgusuario}`);
        }else{
          let resPk = await  this.turnoService.turnoPk(res.listaTurno[0].pk);
            if(resPk.listaTurnopk){
              localStorage.setItem('turnoticket',  resPk.listaTurnopk[0].id);
              this.loading = false;
              this.router.navigate(['/turno']);  
            }else{
              this.sweetAlert.swalConfirm(res.message).then(resolve =>{
                if(resolve)
                  this.rutTurno();
               });
            }
          }   
    }
    
  }


}
