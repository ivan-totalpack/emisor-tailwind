import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class SweetalertService {

  swalConfirm(text:string): Promise<any> {
    return new Promise<boolean>(async resolve => {
      Swal.fire({
        title: '&#x1f625;',
        text: `${text}`,
        confirmButtonText:'Reintentar!',
        confirmButtonColor:'#2563eb'
      }).then((result) => {
        if (result.isConfirmed) {
          resolve(true);
          }else{
            resolve(false);
          }
      })
    }) 
  } 

  toast(text:string): void {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    
    Toast.fire({
      icon: 'error',
      title: text
    })
  }

  swalClose(): void {
    Swal.close();
  }

}
