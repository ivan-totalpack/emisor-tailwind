import { Route } from '@angular/router';

export const routes: Route[] = [

  {
    title:'Series',
    path: '',
    loadComponent:() => import('./components/series/series.component').then(c => c.SeriesComponent),
  },

  {
    title:'Rut',
    path: 'rut',
    loadComponent:() => import('./components/rut/rut.component').then(c => c.RutComponent),
  },

  {
    title:'Telefono',
    path: 'telefono',
    loadComponent:() => import('./components/telefono/telefono.component').then(c => c.TelefonoComponent),
  },

  {
    title:'Turno',
    path: 'turno',
    loadComponent:() => import('./components/turno/turno.component').then(c => c.TurnoComponent),
  },
  
];

