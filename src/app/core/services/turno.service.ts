import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { lastValueFrom} from 'rxjs';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;
const idEmpresa = environment.idEmpresa;
const idOficina = environment.idOficina 
@Injectable({
  providedIn: 'root'
})
export class TurnoService {

  constructor(private http: HttpClient) {}

  headers = new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  })

  async turnoPk(pk:string): Promise<any>{
    let codepk = { "pk": pk };

    try {
      return await lastValueFrom(this.http.post(`${apiUrl}/Web/v3/seguirpk`, codepk, { headers: this.headers}));
    } catch (error) {
      return { status: false, message: 'Ha ocurrido un error al solicitar codigo de turno.' };
    }
  }

  async obtenerTurno(turnoid:any): Promise<any>{
    let id = { "id": turnoid };

    try {
      return await lastValueFrom(this.http.post(`${apiUrl}/Web/v3/seguir`, id, { headers: this.headers}));
    } catch (error) {
      return { status: false, message: 'Ha ocurrido un error al obtener codigo de turno.' };
    }
  }

  async poolqr(data:any): Promise<any>{
    let dataPool = {
      "idempresa": data.idEmpresa,
      "idoficina": data.idOficina
    }

    try {
      return await lastValueFrom(this.http.post(`${apiUrl}/Web/v3/poolqr`, dataPool, { headers: this.headers}));
    } catch (error) {
      return { status: false, message: 'Ha ocurrido un error al verificar pool.' };
    }
  }
  
}
