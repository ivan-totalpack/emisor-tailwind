import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { lastValueFrom} from 'rxjs';
import { environment } from 'src/environments/environment';

const apiUrl = environment.apiUrl;
const idEmpresa = environment.idEmpresa;

@Injectable({
  providedIn: 'root'
})
export class SeriesService {

  constructor(private http: HttpClient) {}

  headers = new HttpHeaders({
    'accept': 'application/json',
    'Content-Type': 'application/json',
  })

  async loginToken(): Promise<any> {
    let data = { email: "turnoonline@totalpack.cl", password: "turno$56.2020"};

    try {
      return await lastValueFrom(this.http.post(`${apiUrl}/v2/identity/login`, data, {headers: this.headers}));
    } catch (error) {
      return { status: false, message: 'Ha ocurrido un error al obtener token.' };
    }
  }

  async listarSeries(): Promise<any> {
    let tokenid:any =  localStorage.getItem('tokenid');
    this.headers = this.headers.set('Authorization', `Bearer ${tokenid}`);
    let id = { "idEmpresa": idEmpresa};

    try {
      return await lastValueFrom(this.http.post(`${apiUrl}/Tol/v2/servicios`, id, {headers: this.headers}));
    } catch (error) {
      return { status: false, message: 'Ha ocurrido un error al obtener servicios.' };
    }
}

}
